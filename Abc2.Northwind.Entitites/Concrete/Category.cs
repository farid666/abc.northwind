﻿using Abc2.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Abc2.Northwind.Entitites.Concrete
{
    public class Category:IEntity
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }

    }
}
