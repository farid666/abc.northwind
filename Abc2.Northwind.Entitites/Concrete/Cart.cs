﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Abc2.Northwind.Entitites.Concrete
{
    public class Cart
    {
        public Cart()
        {
            CartLines = new List<CartLine>();
        }
        public List<CartLine> CartLines { get; set; }
        public decimal TotalAmount
        {
            get { return CartLines.Sum(i => i.Product.UnitPrice * i.Quantity); }
        }

    }
}
