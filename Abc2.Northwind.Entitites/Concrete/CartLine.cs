﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Abc2.Northwind.Entitites.Concrete
{
    public class CartLine
    {
        public Product Product { get; set; }
        public int Quantity { get; set; }

    }
}
