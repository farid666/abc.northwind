﻿using Abc2.Core.DataAcccess.EntityFramework;
using Abc2.Northwind.DataAccess.Abstract;
using Abc2.Northwind.Entitites.Concrete;
using System;
using System.Collections.Generic;
using System.Text;

namespace Abc2.Northwind.DataAccess.Concrete.EntityFramework
{
    public class ProductDal:EFEntityRepositoryBase<Product,NorthwindContext>,IProductDal
    {
    }
}
