﻿using Abc2.Core.DataAcccess;
using Abc2.Northwind.Entitites.Concrete;
using System;
using System.Collections.Generic;
using System.Text;

namespace Abc2.Northwind.DataAccess.Abstract
{
    public interface IProductDal:IEntityRepository<Product>
    {

    }
}
