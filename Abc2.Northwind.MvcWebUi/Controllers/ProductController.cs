﻿using Abc2.Northwind.Business.Abstract;
using Abc2.Northwind.MvcWebUi.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Abc2.Northwind.MvcWebUi.Controllers
{
    public class ProductController : Controller
    {

        private IProductService _productService;

        public ProductController(IProductService productService)
        {
            _productService = productService;
        }

        public IActionResult Index(int page = 1,int categoryId = 0)
        {
            int pageSize = 10;
            var products = _productService.GetByCategoryId(categoryId);
            ProductListViewModel model = new ProductListViewModel
            {
                Products = products.Skip((page - 1) * pageSize).Take(pageSize).ToList(),
                PageCount = (int)Math.Ceiling(products.Count/(double)pageSize),
                PageSize = pageSize,
                CurrentPage = page,
                CurrentCategoryId = categoryId
            };
            return View(model);
        }
    }
}
