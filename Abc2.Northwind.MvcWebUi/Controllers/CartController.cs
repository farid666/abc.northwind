﻿using Abc2.Northwind.Business.Abstract;
using Abc2.Northwind.Entitites.Concrete;
using Abc2.Northwind.MvcWebUi.Models;
using Abc2.Northwind.MvcWebUi.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Abc2.Northwind.MvcWebUi.Controllers
{
    public class CartController : Controller
    {

        private ICartSessionService _cartSessionService;
        private ICartService _cartService;
        private IProductService _productService;

        public CartController(ICartSessionService cartSessionService,ICartService cartService,IProductService productService)
        {
            _cartSessionService = cartSessionService;
            _cartService = cartService;
            _productService = productService;
        }

        public IActionResult AddToCart(int productId)
        {
            var product = _productService.GetById(productId);
            var cart = _cartSessionService.GetCart();
            _cartService.AddToCart(cart, product);
            _cartSessionService.SetCart(cart);

            TempData["message"] = String.Format("Your product, {0} , was successfully added to the cart!",product.ProductName);

            return RedirectToAction("Index", "Product");
        }

        public IActionResult CartList()
        {
            var cart = _cartSessionService.GetCart();

            var model = new CartSummaryViewModel()
            {
                Cart = cart
            };

            return View(model);
        }

        public IActionResult Remove(int productId)
        {
            var cart = _cartSessionService.GetCart();
            _cartService.RemoveFromCart(cart, productId);
            _cartSessionService.SetCart(cart);
            return RedirectToAction("CartList");
        }

        [HttpGet]
        public IActionResult Complete()
        {
            var model = new ShippingDetailsViewModel()
            {
                ShippingDetails = new ShippingDetails()
            };

            return View(model);
        }

        [HttpPost]
        public IActionResult Complete(ShippingDetails shippingDetails)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            TempData["message"] = String.Format("Thank you {0}, your order is in process", shippingDetails.FirstName);
            return View();
        }
    }
}
