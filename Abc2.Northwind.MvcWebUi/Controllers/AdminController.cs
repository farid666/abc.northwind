﻿using Abc2.Northwind.Business.Abstract;
using Abc2.Northwind.Entitites.Concrete;
using Abc2.Northwind.MvcWebUi.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Abc2.Northwind.MvcWebUi.Controllers
{
    [Authorize]
    public class AdminController : Controller
    {

        private IProductService _productService;
        private ICategoryService _categoryService;

        public AdminController(IProductService productService,ICategoryService categoryService)
        {
            _productService = productService;
            _categoryService = categoryService;
        }

        public IActionResult Index()
        {
            var model = new ProductListViewModel()
            {
                Products = _productService.GetAll()
            };

            return View(model);
        }

        [HttpGet]
        public IActionResult Add()
        {
            var model = new ProductAddViewModel()
            {
                Product = new Product(),
                Categories = _categoryService.GetAll()
            };

            return View(model);
        }

        [HttpPost]
        public IActionResult Add(Product product)
        {
            if (ModelState.IsValid)
            {
                _productService.Add(product);
                TempData["message"] = String.Format("Your product {0} succesfully added",product.ProductName);
            }

            return RedirectToAction("Add");
        }

        [HttpGet]
        public IActionResult Update(int productId)
        {
            var model = new ProductUpdateViewModel()
            {
                Product = _productService.GetById(productId),
                Categories = _categoryService.GetAll()
            };

            return View(model);
        }

        [HttpPost]
        public IActionResult Update(Product product)
        {
            if (ModelState.IsValid)
            {
                _productService.Update(product);
                TempData["message"] = String.Format("Your product {0} succesfully updated", product.ProductName);
            }

            return RedirectToAction("Update");
        }

        public IActionResult Delete(int productId)
        {
            _productService.Delete(productId);
            TempData["message"] = String.Format("Your product succesfully deleted");

            return RedirectToAction("Index");
        }
    }
}
