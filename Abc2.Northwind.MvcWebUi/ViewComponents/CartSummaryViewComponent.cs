﻿using Abc2.Northwind.MvcWebUi.Models;
using Abc2.Northwind.MvcWebUi.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Abc2.Northwind.MvcWebUi.ViewComponents
{
    public class CartSummaryViewComponent:ViewComponent
    {

        private ICartSessionService _cartSessionService;

        public CartSummaryViewComponent(ICartSessionService cartSessionService)
        {
            _cartSessionService = cartSessionService;
        }

        public IViewComponentResult Invoke()
        {
            var cart = _cartSessionService.GetCart();

            var model = new CartSummaryViewModel()
            {
                Cart = cart
            };

            return View(model);
        }
    }
}
