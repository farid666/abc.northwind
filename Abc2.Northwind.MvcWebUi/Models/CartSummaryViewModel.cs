﻿using Abc2.Northwind.Entitites.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Abc2.Northwind.MvcWebUi.Models
{
    public class CartSummaryViewModel
    {
        public Cart Cart { get; set; }

    }
}
