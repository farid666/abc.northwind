﻿using Abc2.Northwind.Entitites.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Abc2.Northwind.MvcWebUi.Models
{
    public class ProductListViewModel
    {
        public List<Product> Products { get; set; }
        public int CurrentCategoryId { get; set; }
        public int CurrentPage { get; set; }
        public int PageSize { get; set; }
        public int PageCount { get; set; }


    }
}
