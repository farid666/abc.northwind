﻿using Abc2.Northwind.Business.Abstract;
using Abc2.Northwind.DataAccess.Abstract;
using Abc2.Northwind.Entitites.Concrete;
using System;
using System.Collections.Generic;
using System.Text;

namespace Abc2.Northwind.Business.Concrete
{
    public class CategoryManager : ICategoryService
    {
        private ICategoryDal _categoryDal;

        public CategoryManager(ICategoryDal categoryDal)
        {
            _categoryDal = categoryDal;
        }

        public List<Category> GetAll()
        {
            return _categoryDal.GetList();
        }
    }
}
