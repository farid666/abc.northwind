﻿using Abc2.Northwind.Entitites.Concrete;
using System;
using System.Collections.Generic;
using System.Text;

namespace Abc2.Northwind.Business.Abstract
{
    public interface ICategoryService
    {
        List<Category> GetAll();
    }
}
